��          T      �       �      �      �   !   �      �        �     �  �     7     J  .   \     �     �  }   �                                        Control Center Disable Window Not sure what to do with value $? Other Desktops Session Name Your window manager was not one of the supported window managers. This window is to provide the same function as the Other Desktops menu Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-15 16:03+0000
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: Portuguese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
 Centro de Controlo Desactivar Janela Não tem a certeza do que fazer com o valor $? Outros Ambientes de Trabalho Nome da Sessão O seu gestor de janelas não é suportado. Esta janela irá fornecer a mesma função que o menu Outros Ambientes de Trabalho 