��          T      �       �      �      �   !   �      �        �       �  +   �  5   �  O     !   U     w  �   �                                        Control Center Disable Window Not sure what to do with value $? Other Desktops Session Name Your window manager was not one of the supported window managers. This window is to provide the same function as the Other Desktops menu Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-15 16:03+0000
Last-Translator: Robin, 2021
Language-Team: Russian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Управление компьютером Не показывать это окно снова. Неясно, что должно произойти со значением $? Прочие интерфейсы Название занятий Выбранное вами управление окнами не поддерживается. Это окно берет на себя функцию "меню" других пользовательских интерфейсов. 