��          T      �       �      �      �   !   �      �        �     �  �     2     F  ,   ^     �     �  �   �                                        Control Center Disable Window Not sure what to do with value $? Other Desktops Session Name Your window manager was not one of the supported window managers. This window is to provide the same function as the Other Desktops menu Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-15 16:03+0000
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: French (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
 Centre de contrôle Désactiver la fenêtre Vous ne savez pas quoi faire de la valeur $? Autres Bureaux Nom de session Votre gestionnaire de fenêtres ne faisait pas partie des gestionnaires de fenêtres pris en charge. Cette fenêtre doit fournir la même fonction que le menu Autres Bureaux 