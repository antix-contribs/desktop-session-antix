��          T      �       �      �      �   !   �      �        �     �  �     E     X  2   k     �     �  �   �                                        Control Center Disable Window Not sure what to do with value $? Other Desktops Session Name Your window manager was not one of the supported window managers. This window is to provide the same function as the Other Desktops menu Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-15 16:03+0000
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: Portuguese (Brazil) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 Centro de Controle Desativar a Janela Você não tem certeza do que fazer com o valor $? Outras Áreas de Trabalho Nome da Sessão O seu gerenciador de janelas não era um dos gerenciadores de janela suportados.
Esta janela irá fornecer a mesma função do menu 'Outras Áreas de Trabalho'. 