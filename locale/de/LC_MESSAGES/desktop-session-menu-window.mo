��          T      �       �      �      �   !   �      �        �     �  �     3     C  2   X     �     �  �   �                                        Control Center Disable Window Not sure what to do with value $? Other Desktops Session Name Your window manager was not one of the supported window managers. This window is to provide the same function as the Other Desktops menu Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-15 16:03+0000
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: German (https://www.transifex.com/antix-linux-community-contributions/teams/120110/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Kontrollzentrum Fenster deaktivieren Es ist unklar, was mit dem Wert $? geschehen soll. Andere Arbeitsoberflächen Sitzungsbezeichnung Die von Ihnen gewählte Fensterverwaltung wird nicht unterstützt. Dieses Fenster übernimmt die Funktion des „Menüs” anderer Arbeitsoberflächen. 