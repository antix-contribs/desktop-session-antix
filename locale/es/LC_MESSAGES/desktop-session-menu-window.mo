��          T      �       �      �      �   !   �      �        �     �  �     4     F  *   [     �     �  �   �                                        Control Center Disable Window Not sure what to do with value $? Other Desktops Session Name Your window manager was not one of the supported window managers. This window is to provide the same function as the Other Desktops menu Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-15 16:03+0000
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: Spanish (https://www.transifex.com/antix-linux-community-contributions/teams/120110/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
 Centro de Control Deshabilitar Ventana No sabemos qué hacer con la selección $? Otros Entornos de Sesión Nombre de la Sesión Su Gestor de Ventanas o Entorno de Escritorio puede no ser compatible. Esta ventana le permite cambiar de sesión, al igual que el menú 'Otros Escritorios'. 